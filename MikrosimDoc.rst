====================================================
Spatial Microsimulation: an Example with German Data 
====================================================


:Author: Esteban Munoz emunozh@gmail.com
:Date: Friday 5th December, 2014
:Prepared for: The AURIN/NATSEM Microsimulation Symposium



Go to: https://github.com/emunozh/GREGWT for an updated version of the GREGWT
library

And to: https://github.com/emunozh/mikrosim for the original version of the
GREGWT library presented at the AURIN/NATSEM Microsimulation Symposium